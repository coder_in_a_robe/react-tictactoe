`RGB(255, 0, 0)`
`RGB(0, 255, 0)`
`RGB(0, 0, 255)`

[[_TOC_]]

# TicTacToe ReactJS Tutorial

Starting the ReactJS tutorial which I've already done at least once before. I need a refresh, so here we go!

## Main Graph to Live By

```mermaid
graph TD;
    A(Follow Tutorial) --> B(Build Tic Tac Toe);
    B --> C(Learn React);
    C --> D[Get Side Tracked];
    C --> E(Work on Python Projects);
    E --> F(Remember That You Started Tic Tac Toe Tutorial);
    E --> D
    F --> C
    F --> G{Become Webdev God};
    D --> G
```

      ___________
     < holy moly >
      -----------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

## The Circle of Life

```mermaid
stateDiagram
    Eat --> Sleep
    Sleep --> Code
    Code --> Eat
```

      ________________
     < you do you boo >
      ----------------
     \                             .       .
      \                           / `.   .' "
       \                  .---.  <    > <    >  .---.
        \                 |    \  \ - ~ ~ - /  /    |
              _____          ..-~             ~-..-~
             |     |   \~~~\.'                    `./~~~/
            ---------   \__/                        \__/
           .'  O    \     /               /       \  "
          (_____,    `._.'               |         }  \/~~~/
           `----.          /       }     |        /    \__/
                 `-.      |       /      |       /      `. ,~~|
                     ~-.__|      /_ - ~ ^|      /- _      `..-'
                          |     /        |     /     ~-.     `-. _  _  _
                          |_____|        |_____|         ~ - . _ _ _ _ _>

## Additions and Subtractions {aka the changelog}

-   [+ Do a thing +]
-   [- Break a thing -]
-   [+ Fix a thing +]
-   [- Cry tears of joy -]
-   [+ Revel in your masterpiece +]

## Revelations

```math
\fcolorbox{red}{blue}{$github \neq gitlab$}\\
\fcolorbox{red}{blue}{$gitlab \gt github$}\\
\fcolorbox{red}{blue}{$github \lt gitlab$}\\
\fcolorbox{red}{blue}{$\int{github}=0$}\\
\fcolorbox{red}{blue}{$\int{gitlab}=\infty$}
```

## Vidjya {Video for the uninitiated}

![Sample Video](./do_thing.gif)
